<a href="https://www.beyondactivismo.org">
  <img
    src="https://beyondactivismo.org/wp-content/uploads/2017/11/LOGO-COMO-LOGO-PAG-copia-768x388.jpg"
    align="center"
    width=400
  </img>
</a>

Roadmap
===

Beyond Activismo is a safe multilingual platform, that works as an open
collaborator network between different people around the world, where on the one
hand we can share, in an attractive and creative way, verified human rights 
activists experiences and histories documented by the BA’s Community. 
And on the other hand, where human rights activists can inspire, interact, collaborate and support each other.


## What do we need to do?

Over the next Weeks we'll be developing a prototype of our collaboration plattform 
to present it at the Mozilla Global Sprint.

## Shortterm: 

Finish a Prototype until Mozilla Global Sprint 11/12 May 2018

Issue #5

## TB-9

First Beta of Readme.md on GitLab
Contact other communities to exchange experiences
Get in touch with designers and website developers

## TB-8

Creating a Vision for the webpage. What functions are needed to make collaboration 
possible?

## TB-7

===






